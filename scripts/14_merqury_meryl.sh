#!/usr/bin/env bash

#SBATCH --time=6:00:00
#SBATCH --mem=16G
#SBATCH --cpus-per-task=4
#SBATCH --job-name=merqury_init
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,fail,end
#SBATCH --output=/data/users/jackermann/merqury_init_out%j.o
#SBATCH --error=/data/users/jackermann/merqury_init_err%j.e
#SBATCH --partition=pall

# Define paths to directories
WORKDIR=/data/users/jackermann/assembly_annotation_course
DATADIR=/data/courses/assembly-annotation-course
dir_reads=${WORKDIR}/participant_4/Illumina
dir_meryl=${WORKDIR}/assemblies/quality_assessment/merqury/meryl

# Go to working directory for meryl
#cd $dir_meryl/

# Prepare meryl dbs
singularity exec \
--bind $WORKDIR \
--bind $DATADIR \
/software/singularity/containers/Merqury-1.3-1.ubuntu20.sif \
meryl union-sum \
 output Ler.meryl \
 [count k=21 ${dir_reads}/ERR3624577_1.fastq.gz output read1.meryl] \
 [count k=21 ${dir_reads}/ERR3624577_2.fastq.gz output read2.meryl]
