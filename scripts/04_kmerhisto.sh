#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=02:00:00
#SBATCH --job-name=kmerhisto
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/output_kmer_histo%j.o
#SBATCH --error=/data/users/jackermann/error_kmer_histo%j.e
#SBATCH --partition=pcourseassembly

module load UHTS/Analysis/jellyfish/2.3.0

WORKDIR=/data/users/jackermann/assembly_annotation_course/read_QC/kmer_counting
cd $WORKDIR

jellyfish histo -t 4 ill_reads.jf > ill_reads.histo
jellyfish histo -t 4 pb_reads.jf > pb_reads.histo
jellyfish histo -t 4 RNAseq_reads.jf > RNAseq_reads.histo
