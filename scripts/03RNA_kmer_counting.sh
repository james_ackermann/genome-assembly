#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=8000M
#SBATCH --time=02:00:00
#SBATCH --job-name=rs_kmercount
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/output_rs_kmercount_%j.o
#SBATCH --error=/data/users/jackermann/error_rs_kmercount_%j.e
#SBATCH --partition=pcourseassembly

module load UHTS/Analysis/jellyfish/2.3.0

WORKDIR=/data/users/jackermann/assembly_annotation_course
READ_DIR=/data/courses/assembly-annotation-course/raw_data/C24/participant_4/

IN=${READ_DIR}/RNAseq

FILES=`ls ${IN}/*.fastq.gz`
SEQS=($FILES)

OUT=${WORKDIR}/read_QC/kmer_counting
cd $OUT

jellyfish count -C -m 21 -s 5G -t 4 <(zcat ${SEQS[1]}) <(zcat ${SEQS[2]}) -o RNAseq_reads.jf
