#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4000M
#SBATCH --time=0:10:00
#SBATCH --job-name=fastqc
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/output_fastqc_%j.o
#SBATCH --error=/data/users/jackermann/error_fastqc_%j.e
#SBATCH --partition=pcourseassembly

HOME=/data/users/jackermann/
mkdir ${HOME}/assembly_annotation_course
WORKDIR=/data/users/jackermann/assembly_annotation_course
cd ${WORKDIR}
ln -s /data/courses/assembly-annotation-course/raw_data/C24/participant_4/ ./

mkdir assemblies
mkdir read_QC
mkdir read_QC/fastqc
mkdir scripts
