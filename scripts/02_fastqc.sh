#!/usr/bin/env bash

#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4000M
#SBATCH --time=01:00:00
#SBATCH --job-name=fastqc
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/output_fastqc_%j.o
#SBATCH --error=/data/users/jackermann/error_fastqc_%j.e
#SBATCH --partition=pcourseassembly

module load UHTS/Quality_control/fastqc/0.11.9

WORKDIR=/data/users/jackermann/assembly_annotation_course
READ_DIR=/data/users/jackermann/assembly_annotation_course/participant_4

#illumina
fastqc -o ${WORKDIR}/read_QC/fastqc  ${READ_DIR}/Illumina/*fastq.gz

#pacbio
fastqc -o ${WORKDIR}/read_QC/fastqc  ${READ_DIR}/pacbio/*fastq.gz

#RNAseq
fastqc -o ${WORKDIR}/read_QC/fastqc  ${READ_DIR}/RNAseq/*fastq.gz
