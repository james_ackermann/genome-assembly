#!/usr/bin/env bash

#SBATCH --time=1-00:00:00
#SBATCH --mem-per-cpu=4G
#SBATCH --cpus-per-task=12
#SBATCH --job-name=trinity_assembly
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/trinity_out%j.o
#SBATCH --error=/data/users/jackermann/trinity_err%j.e
#SBATCH --partition=pcourseassembly

module load UHTS/Assembler/trinityrnaseq/2.5.1;

WORKDIR=/data/users/jackermann/assembly_annotation_course
READ_DIR=/data/users/jackermann/assembly_annotation_course/participant_4/RNAseq

cd ${WORKDIR}/assemblies
mkdir trinity
cd trinity

Trinity --seqType fq --max_memory 48G \
--single ${READ_DIR}/SRR1584462_1.fastq.gz,${READ_DIR}/SRR1584462_2.fastq.gz \
--CPU 12
