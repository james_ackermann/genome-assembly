#!/usr/bin/env bash
#SBATCH --time=12:00:00
#SBATCH --mem=64G
#SBATCH --cpus-per-task=2
#SBATCH --job-name=pilon
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/pilon_out%j.o
#SBATCH --error=/data/users/jackermann/pilon_err%j.e
#SBATCH --partition=pall

WORKDIR=/data/users/jackermann/assembly_annotation_course/assemblies

cd ${WORKDIR}/flye_pacbio

java -Xmx45g -jar /mnt/software/UHTS/Analysis/pilon/1.22/bin/pilon-1.22.jar --genome assembly.fasta --frags flye.bam
