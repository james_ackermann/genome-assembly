#!/usr/bin/env bash
#SBATCH --time=08:00:00
#SBATCH --mem-per-cpu=16G
#SBATCH --cpus-per-task=2
#SBATCH --job-name=bwt-index
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/bowtieI_out%j.o
#SBATCH --error=/data/users/jackermann/bowtieI_err%j.e
#SBATCH --partition=pcourseassembly

module add UHTS/Aligner/bowtie2/2.3.4.1;

WORKDIR=/data/users/jackermann/assembly_annotation_course/assemblies
READ_DIR=/data/users/jackermann/assembly_annotation_course/participant_4/Illumina

cd ${WORKDIR}/flye_pacbio

bowtie2-build assembly.fasta flyeindex

cd ${WORKDIR}/canu_pacbio

bowtie2-build C24_p4.contigs.fasta canuindex
