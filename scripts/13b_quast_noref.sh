#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem=64G
#SBATCH --time=1-00:00:00
#SBATCH --job-name=quast_noref
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/quastnoref_out%j.o
#SBATCH --error=/data/users/jackermann/quastnoref_err%j.e
#SBATCH --partition=pall

# Define paths to directories
WORKDIR=/data/users/jackermann/assembly_annotation_course
DATADIR=/data/courses/assembly-annotation-course

# Go to working directory
cd ${WORKDIR}/assemblies/quast/
mkdir no_reference
cd no_reference

# Run QUAST with reference
singularity exec \
--bind $WORKDIR \
--bind $DATADIR \
/data/courses/assembly-annotation-course/containers/quast_5.1.0rc1.sif \
quast.py ${WORKDIR}/assemblies/flye_pacbio/assembly.fasta \
 ${WORKDIR}/assemblies/flye_pacbio/pilon.fasta \
 ${WORKDIR}/assemblies/canu_pacbio/C24_p4.contigs.fasta \
 ${WORKDIR}/assemblies/canu_pacbio/pilon.fasta \
 --large \
 --threads 4 \
  --est-ref-size 125000000 \
 --labels "flye initial","flye polished","canu initial","canu polished" \
 -o ${WORKDIR}/assemblies/quast/with_reference/ \
 --pacbio ${WORKDIR}/participant_4/pacbio/ERR3415819.fastq.gz \
 --pacbio ${WORKDIR}/participant_4/pacbio/ERR3415820.fastq.gz \
 --no-sv
