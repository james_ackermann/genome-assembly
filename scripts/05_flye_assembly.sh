#!/bin/sh
#SBATCH --time=08:00:00
#SBATCH --mem-per-cpu=4G
#SBATCH --cpus-per-task=16
#SBATCH --job-name=flye_assembly
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/flye_out%j.o
#SBATCH --error=/data/users/jackermann/flye_err%j.e
#SBATCH --partition=pall

module load UHTS/Assembler/flye/2.8.3;

WORKDIR=/data/users/jackermann/assembly_annotation_course
READ_DIR=/data/users/jackermann/assembly_annotation_course/participant_4/pacbio

flye --pacbio-raw ${READ_DIR}/* --out-dir ${WORKDIR}/assemblies/flye_pacbio --threads 16
