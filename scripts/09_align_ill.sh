#!/usr/bin/env bash
#SBATCH --time=08:00:00
#SBATCH --mem=64G
#SBATCH --cpus-per-task=2
#SBATCH --job-name=bt2align
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/bt2align_out%j.o
#SBATCH --error=/data/users/jackermann/bt2align_err%j.e
#SBATCH --partition=pall

module add UHTS/Aligner/bowtie2/2.3.4.1;

WORKDIR=/data/users/jackermann/assembly_annotation_course/assemblies
READ_DIR=/data/users/jackermann/assembly_annotation_course/participant_4/Illumina

cd ${WORKDIR}/flye_pacbio

bowtie2 --sensitive-local -x flyeindex -1 <(zcat ${READ_DIR}/ERR3624577_1.fastq.gz) -2 <(zcat ${READ_DIR}/ERR3624577_2.fastq.gz) -S flye.sam
