#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=16G
#SBATCH --time=10:00:00
#SBATCH --job-name=busco_pol
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/busco_p_out%j.o
#SBATCH --error=/data/users/jackermann/busco_p_out%j.e
#SBATCH --partition=pall

# busco polished flye and canu #

WORKDIR=/data/users/jackermann/assembly_annotation_course/assemblies

#flye
singularity exec --bind ${WORKDIR}/flye_pacbio /data/courses/assembly-annotation-course/containers2/busco_v5.1.2_cv1.sif busco --cpu 4 -m genome -i ${WORKDIR}/flye_pacbio/pilon.fasta -o busco_flye -l brassicales_odb10

#canu
singularity exec --bind ${WORKDIR}/canu_pacbio /data/courses/assembly-annotation-course/containers2/busco_v5.1.2_cv1.sif busco --cpu 4 -m genome -i ${WORKDIR}/canu_pacbio/pilon.fasta -o busco_canu  -l brassicales_odb10
