#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=8G
#SBATCH --time=6:00:00
#SBATCH --job-name=busco_plots
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/busco_plots_out%j.o
#SBATCH --error=/data/users/jackermann/busco_plots_err%j.e
#SBATCH --partition=pall

WORKDIR=/data/users/jackermann/assembly_annotation_course/assemblies
OUTDIR=${WORKDIR}/quality_assessment/busco

cd ${OUTDIR}

singularity exec --bind ${OUTDIR} /data/courses/assembly-annotation-course/containers2/busco_v5.1.2_cv1.sif ${OUTDIR}/generate_plot.py -wd ${OUTDIR}/BUSCO_summaries
