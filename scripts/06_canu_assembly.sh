#!/usr/bin/env bash
#SBATCH --time=01:00:00
#SBATCH --mem-per-cpu=4G
#SBATCH --cpus-per-task=1
#SBATCH --job-name=canu_assembly
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/canu_out%j.o
#SBATCH --error=/data/users/jackermann/canu_err%j.e
#SBATCH --partition=pcourseassembly

module load UHTS/Assembler/canu/2.1.1

WORKDIR=/data/users/jackermann/assembly_annotation_course
READ_DIR=/data/users/jackermann/assembly_annotation_course/participant_4/pacbio

canu -p C24_p4 -d ${WORKDIR}/assemblies/canu_pacbio genomeSize=125m maxThreads=16 maxMemory=64 gridEngineResourceOption="--cpus-per-task=THREADS --mem-per-cpu=MEMORY" gridOptions="--partition=pcourseassembly --mail-user=james.ackermann@students.unibe.ch" -pacbio ${READ_DIR}/*
