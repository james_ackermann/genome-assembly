#!/usr/bin/env bash

#SBATCH --time=1-00:00:00
#SBATCH --mem=16G
#SBATCH --cpus-per-task=4
#SBATCH --job-name=merqury_canu
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,fail,end
#SBATCH --output=/data/users/jackermann/merqury_canu_out%j.o
#SBATCH --error=/data/users/jackermann/merqury_canu_err%j.e
#SBATCH --partition=pall

# Define paths to directories
WORKDIR=/data/users/jackermann/assembly_annotation_course
DATADIR=/data/courses/assembly-annotation-course
dir_meryl=${WORKDIR}/assemblies/quality_assessment/merqury/meryl

# Go to working directory for merqury
cd ${WORKDIR}/assemblies/quality_assessment/merqury/canu

# Run the container with merqury
singularity exec \
--bind $WORKDIR \
/software/singularity/containers/Merqury-1.3-1.ubuntu20.sif \
merqury.sh \
$dir_meryl/C24.meryl \
${WORKDIR}/assemblies/canu_pacbio/C24_p4.contigs.fasta \
canu_initial

# Run the container with merqury
singularity exec \
--bind $WORKDIR \
/software/singularity/containers/Merqury-1.3-1.ubuntu20.sif \
merqury.sh \
$dir_meryl/C24.meryl \
${WORKDIR}/assemblies/canu_pacbio/pilon.fasta \
canu_polished
