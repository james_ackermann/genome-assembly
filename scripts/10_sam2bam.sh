#!/usr/bin/env bash
#SBATCH --time=01:00:00
#SBATCH --mem=64G
#SBATCH --cpus-per-task=4
#SBATCH --job-name=sam2bam
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/sam2bam_out%j.o
#SBATCH --error=/data/users/jackermann/sam2bam_err%j.e
#SBATCH --partition=pall

module load UHTS/Analysis/samtools/1.10;

WORKDIR=/data/users/jackermann/assembly_annotation_course/assemblies

cd ${WORKDIR}/flye_pacbio

samtools sort -T $SCRATCH -@ $SLURM_CPUS_PER_TASK flye.sam -o flye_sorted.sam
samtools view -bS flye_sorted.sam -o flye.bam
samtools index flye.bam
