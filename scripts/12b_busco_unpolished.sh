#!/usr/bin/env bash

#SBATCH --cpus-per-task=4
#SBATCH --mem-per-cpu=16G
#SBATCH --time=12:00:00
#SBATCH --job-name=busco_unpol
#SBATCH --mail-user=james.ackermann@students.unibe.ch
#SBATCH --mail-type=begin,end
#SBATCH --output=/data/users/jackermann/busco_u_out%j.o
#SBATCH --error=/data/users/jackermann/busco_u_err%j.e
#SBATCH --partition=pall

WORKDIR=/data/users/jackermann/assembly_annotation_course/assemblies
OUTDIR=${WORKDIR}/quality_assessment/busco

cd ${OUTDIR}

#trinity
singularity exec --bind ${WORKDIR}/trinity/,${OUTDIR} /data/courses/assembly-annotation-course/containers2/busco_v5.1.2_cv1.sif busco --cpu 4 -m transcriptome -i ${WORKDIR}/trinity/trinity_out_dir/Trinity.fasta -o busco_trinity -l brassicales_odb10
